package com.mathi.recyclerviewsample

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast

/**
 * Created by Mathiazhagan on 20-06-2017.
 */

class CustomAdapter(internal var context: Context, internal var androidList: List<Android>) : RecyclerView.Adapter<CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.layout_android_list, parent, false)

        return CustomViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {

        holder.name.text = androidList[position].name
        holder.api.text = context.getString(R.string.api) + ": " + androidList[position].api
        holder.version.text = context.getString(R.string.version) + ": " + androidList[position].verison

        holder.androidLayout.setOnClickListener { Toast.makeText(context, holder.name.text, Toast.LENGTH_SHORT).show() }

    }

    override fun getItemCount(): Int {
        return androidList.size
    }
}
